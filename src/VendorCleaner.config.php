<?php

return array(

    # Laravel 4 默认安装的资源包
    'filp/whoops'               => 'README.md phpunit.xml* tests examples LICENSE.md',
    'ircmaxell/password-compat' => 'README.md test',
    'laravel/framework'         => 'build tests .scrutinizer.yml .travis.yml CONTRIBUTING.md LICENSE.txt phpubit.php phpunit.xml readme.md',
    'nikic/php-parser'          => 'README.md CHANGELOG* phpunit.xml* doc test test_old',
    'patchwork/utf8'            => 'README.md tests',
    'predis/predis'             => 'README.md CHANGELOG* phpunit.xml* examples tests bin FAQ CONTRIBUTING*',
    'swiftmailer/swiftmailer'   => 'CHANGES README* build* doc docs notes test-suite tests create_pear_package.php package*',

    'symfony/browser-kit/Symfony/Component/BrowserKit'           => 'CHANGELOG* README* Tests',
    'symfony/console/Symfony/Component/Console'                  => 'CHANGELOG* README* Tests',
    'symfony/css-selector/Symfony/Component/CssSelector'         => 'CHANGELOG* README* Tests',
    'symfony/debug/Symfony/Component/Debug'                      => 'CHANGELOG* README* Tests',
    'symfony/dom-crawler/Symfony/Component/DomCrawler'           => 'CHANGELOG* README* Tests',
    'symfony/event-dispatcher/Symfony/Component/EventDispatcher' => 'CHANGELOG* README* Tests',
    'symfony/filesystem/Symfony/Component/Filesystem'            => 'CHANGELOG* README* Tests',
    'symfony/finder/Symfony/Component/Finder'                    => 'CHANGELOG* README* Tests',
    'symfony/http-foundation/Symfony/Component/HttpFoundation'   => 'CHANGELOG* README* Tests',
    'symfony/http-kernel/Symfony/Component/HttpKernel'           => 'CHANGELOG* README* Tests',
    'symfony/process/Symfony/Component/Process'                  => 'CHANGELOG* README* Tests',
    'symfony/routing/Symfony/Component/Routing'                  => 'CHANGELOG* README* Tests',
    'symfony/translation/Symfony/Component/Translation'          => 'CHANGELOG* README* Tests',
    'symfony/yaml'                                               => 'CHANGELOG* README* Tests',
    'symfony/filesystem'                                         => 'CHANGELOG* README* Tests',
    'symfony/translation'                                        => 'CHANGELOG* README* Tests',
    'symfony/security-core'                                      => 'CHANGELOG* README* Tests',
    'symfony/routing'                                            => 'CHANGELOG* README* Tests',
    'symfony/process'                                            => 'CHANGELOG* README* Tests',
    'symfony/http-kernel'                                        => 'CHANGELOG* README* Tests',
    'symfony/event-foundation'                                   => 'CHANGELOG* README* Tests',
    'symfony/finder'                                             => 'CHANGELOG* README* Tests',
    'symfony/css-selector'                                       => 'CHANGELOG* README* Tests',
    'symfony/console'                                            => 'CHANGELOG* README* Tests',
    'symfony/debug'                                              => 'CHANGELOG* README* Tests',
    'symfony/browser-kit'                                        => 'CHANGELOG* README* Tests',
    'symfony/dom-crawler'                                        => 'CHANGELOG* README* Tests',


    #PHPunit 套件包
    'phpunit/php-code-coverage'                                  => 'tests',
    'phpunit/timer'                                              => 'tests',
    'phpunit/php-token-steam'                                    => 'tests',
    'phpunit/phpunit'                                            => 'tests',
    'phpunit/phpunit-mock-objects'                               => 'tests',


    #Doctrine 套件包
    'doctrine/annotations'                                       => 'bin tests',
    'doctrine/cache'                                             => 'bin tests',
    'doctrine/collections'                                       => 'tests',
    'doctrine/common'                                            => 'README* UPGRADE* phpunit.xml* build* tests bin lib/vendor',
    'doctrine/dbal'                                              => 'bin build* docs docs2 tests lib/vendor',
    'doctrine/inflector'                                         => 'phpunit.xml* README* tests',
    'doctrine/instantiator'                                      => 'tests .scrutinizer.yml .travis.install.sh .travis.yml CONTRIBUTING.md LICENSE phpmd.xml.dist phpunit.xml.dist README.md',


    #Sebastian 套件包
    'sebastian/comparator'                                       => 'tests',
    'sebastian/diff'                                             => 'tests',
    'sebastian/environment'                                      => 'tests',
    'sebastian/exporter'                                         => 'tests',
    'sebastian/global-state'                                     => 'tests',
    'sebastian/recursion-content'                                => 'tests',


    # 常用资源包
    'barryvdh/laravel-debugbar'                                  => 'readme.md phpunit.xml tests',
    'anahkiasen/former'                                          => 'README* CHANGELOG* CONTRIBUTING* phpunit.xml* tests',
    'anahkiasen/html-object'                                     => 'README* CHANGELOG* phpunit.xml* examples tests',
    'anahkiasen/underscore-php'                                  => 'README* CHANGELOG* phpunit.xml* tests',
    'intervention/image'                                         => 'README* phpunit.xml* public tests',
    'jasonlewis/basset'                                          => 'README* phpunit.xml* tests/Basset',
    'leafo/lessphp'                                              => 'README* docs tests Makefile package.sh',
    'kriswallsmith/assetic'                                      => 'CHANGELOG* phpunit.xml* tests docs',
    'phpoffice/phpexcel'                                         => 'Examples unitTests changelog.txt license.md install.txt',
    'phenx/php-font-lib'                                         => 'www',
    'mustache/mustache'                                          => 'bin test',
    'mockery/mockery'                                            => 'examples tests docs travis .php_cs',
    'dompdf/dompdf'                                              => 'www',
    'mrclay/minify'                                              => 'HISTORY* MIN.txt UPGRADING* README* min_extras min_unit_tests min/builder min/config* min/quick-test* min/utils.php min/groupsConfig.php min/index.php',
    'jsnlib/jsnao'                                               => 'README.md DEMO',
    'phpmailer/phpmailer'                                        => 'LICENSE examples',
    'fzaninotto/faker'                                           => 'README.md LICENSE CHANGELOG.md CONTRIBUTING.md tests Makefile phpunit.xml.dist readme.md .travis.yml',
    'monolog/monolog'                                            => 'tests doc README.markdown phpunit.xml*',
    'psr/log'                                                    => 'Test',


    # 其它资源包
    'phpdocumentor/reflection-docblock'                          => 'README* CHANGELOG* phpunit.xml* tests',
    'rcrowe/twigbridge'                                          => 'README* CHANGELOG* phpunit.xml* tests',
    'twig/twig'                                                  => 'README* CHANGELOG* phpunit.xml* test doc',
    'cartalyst/sentry'                                           => 'README* CHANGELOG* phpunit.xml* tests docs',
    'maximebf/debugbar'                                          => 'README* CHANGELOG* phpunit.xml* tests demo docs',
    'dflydev/markdown'                                           => 'README* CHANGELOG* phpunit.xml* tests',
    'jeremeamia/SuperClosure'                                    => 'README* CHANGELOG* phpunit.xml* tests demo',
    'nesbot/carbon'                                              => 'README* CHANGELOG* phpunit.xml* tests',


    # 自訂套件包
    'alexgarrett/violin'                                         => 'README.md examples tests',
    'carlosocarvalho/simple-input'                               => 'README.md LICENSE',
    'clue/stream-filter'                                         => 'README.md CHANGELOG.md LICENSE tests',
    'container-interop/container-interop'                        => 'README.md LICENSE docs',
    'davedevelopment/phpmig'                                     => 'README.md CHANGELOG.md LICENSE bin tests',
    'filipac/ip'                                                 => 'readme.md',
    'filp/whoops'                                                => 'LICENSE.md',
    'five-say/vendor-cleaner'                                    => 'README.md',
    'guzzlehttp/guzzle'                                          => 'README.md LICENSE CHANGELOG.md UPGRADING.me build docs tests',
    'guzzlehttp/promises'                                        => 'README.md LICENSE CHANGELOG.md',
    'guzzlehttp/psr7'                                            => 'README.md LICENSE CHANGELOG.md',
    'mailgun/mailgun-php'                                        => 'README.md LICENSE CHANGELOG.md',
    'monolog/monolog'                                            => 'README.md LICENSE CHANGELOG.md',
    'nesbot/carbon'                                              => 'README.md LICENSE',
    'nikic/fast-route'                                           => 'README.md LICENSE phpunit.xml',
    'paragonie/random_compat'                                    => 'README.md LICENSE CHANGELOG.md RATIONALE.md SECURITY.md dist other',
    'pimple/pimple'                                              => 'LICENSE CHANGELOG.md ext',
    'webmozart/assert'                                           => 'README.md LICENSE CHANGELOG.md tests',
    'windwalker/crypt'                                           => 'README.md Test',
    'zeuxisoo/slim-whoops'                                       => 'README.md examples test',


    # Laravel 4 自訂資源包
    'caouecs/laravel4-lang'                                      => 'README.md Source.md Status.md Changelog.md',
    'classpreloader/classpreloader'                              => 'README.md LICENSE.md',
    'd11wtq/boris'                                               => 'README.md LICENSE',
    'dingo/api'                                                  => 'readme.md changes.md phpunit.xml',
    'hamcrest/hamcrest-php'                                      => 'tests .coveralls.yml gush.yml .teavis.yml CHANGES.txt LICENSE.txt README.md TODO,txt',
    'indatus/dispatcher'                                         => 'tests .sscrutinizer.yml .travis.composer.config.json .travis.yml LICENSE phpunit.hhvm.xml phpunit.xml README.md travis.php.ini',
    'intervention/image'                                         => 'LICENSE provides.json',
    'ircmaxell/password-compat'                                  => 'LICENSE.md version-test.php',
    'jeremeamia/SuperClosure'                                    => 'travis.yml LICENSE.md',
    'laracasts/flash'                                            => '.travis.yml phpunit.xml readme.md',
    'league/fractal'                                             => 'LICENSE',
    'mtdiwling/cron-expression'                                  => 'tests README.md LICENSE',
    'pqb/filemanger-laravel'                                     => 'tests README.md LICENSE',
    'wp-cli/php-cli-tools'                                       => 'examples README.md test.php LICENSE',
    'folklore/image'                                             => 'tests readme.md phpunit.xml',
    'yajra/laravel-oci8'                                         => 'tests',
    'mtdowling/cron-exoression'                                  => 'tests',
    'hamcrest/hamcrest-php'                                      => 'tests',


);

<?php

namespace Tpk;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class VendorCleaner
{
    private static $vendorDir;
    private static $topDir;
    private static $backupDir;
    private static $Filesystem;

    public function __construct ()
    {
        self::$vendorDir = dirname(dirname(dirname(__DIR__)));
        self::$topDir = dirname(self::$vendorDir);
        self::$backupDir = self::$topDir . '/VendorCleanerBackup';
        self::$Filesystem = new Filesystem();
    }

    /**
     * 将非必要文件转移至备份文件夹，以减小 vendor 目录体积
     * @return void
     */
    public static function backup ()
    {
        new VendorCleaner();
        // 獲取主要規則文件
        // 若 vendor 同级目錄下的 bootstrap 中存在用户自定義文件，則使用用户自定義文件
        if (self::$Filesystem->exists(self::$topDir . '/bootstrap/VendorCleaner.php'))
            $rules = require self::$topDir . '/bootstrap/VendorCleaner.php';
        else
            $rules = require 'VendorCleaner.config.php';

        // 分别處理各个資源包
        foreach ($rules as $packageDir => $rule) {
            if (!file_exists(self::$vendorDir . '/' . $packageDir)) continue;
            // 拆分子規則
            $patterns = explode(' ', $rule);
            // 執行拆分后的規則
            foreach ($patterns as $pattern) {
                $Finder = new Finder();
                try {
                    foreach ($Finder->name($pattern)->in(self::$vendorDir . '/' . $packageDir) as $file) {
                        $backup = str_replace(self::$vendorDir, self::$backupDir, $file);
                        if ($file->isDir()) {
                            // 文件夹處理
                            self::$Filesystem->copyDirectory($file, $backup);
                            self::$Filesystem->deleteDirectory($file);
                        } else if ($file->isFile()) {
                            // 文件處理

                            //針對不同OS，替換路徑分割線
                            $folder = explode('/', str_replace((PHP_OS == 'LINUX') ? '/' : '\\', '/', $backup));

                            //刪除路徑最後一筆(檔案)
                            array_pop($folder);

                            //判斷是否有該路徑
                            if (!file_exists(implode('/', $folder))) {

                                self::Make_Folder($folder);

                            }

                            self::$Filesystem->move($file, $backup);
                        }
                    }
                } catch (\Exception $e) {
                }
            }
        }

    }

    public static function Make_Folder ($folder)
    {

        $target = array();
        foreach ($folder as $index => $item) {
            if (file_exists((empty($target) ? $item : implode('/', $target)))) {
                array_push($target, $item);
            } else {
                self::$Filesystem->makeDirectory(implode('/', $target));
                self::$Filesystem->makeDirectory(implode('/', $target) . '/' . $item);
            }
        }

        if(count($target) === count($folder) && !file_exists(implode('/', $target))){
            self::$Filesystem->makeDirectory(implode('/', $target));
        }
    }

    /**
     * 以備份文件夾恢復原文件，防止資源包更新時浪費時間做多餘的下載
     * @return void
     */
    public static function restore ()
    {
        new VendorCleaner();
        if (self::$Filesystem->exists(self::$backupDir)) {
            // 文件恢复
            self::$Filesystem->copyDirectory(self::$backupDir, self::$vendorDir);
            self::$Filesystem->deleteDirectory(self::$backupDir);
        }
    }
}